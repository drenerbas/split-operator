import numpy as np
from numpy.polynomial.hermite import Hermite
from numpy.polynomial.laguerre import Laguerre
from potential import HarmonicOscialltor, QuarticDoubleWell, MorseOscillator


class ExactSolution(object):
    """Abstract base class for exact solutions to the schrodinger equation
    """

    def energy_level(self, n):
        raise NotImplementedError

    def eigenfunction(self, n, X):
        raise NotImplementedError

    def wavepacket_propagation(self, n, X0):
        """
        Use the first n time independant eigenstates of the potential as a basis
        in which to solve the time dependent schrodinger equation and
        propagate the initial wavepacket passed in as X0
        """
        pass


class ExactHarmonicOscillator(ExactSolution):
    """
    Class to compute exact solutions of a harmonic oscillator.
    The constructor takes an instance of the HarmonicOscillator class
    and uses it's parameters
    """

    def __init__(self, ho):
        if not isinstance(ho, HarmonicOscialltor):
            raise TypeError("You must pass an instance of a HarmonicOscialltor")
        self.w = ho.w
        self.m = ho.m

    def energy_level(self, n):
        return (2*n + 1)*self.w/2.0

    def eigenfunction(self, n, X):
        psi_norm = (self.m*self.w/np.pi)**0.25
        psi_norm *= 1.0/np.sqrt(np.math.factorial(n)*2.0**n)
        H = Hermite.basis(n)
        psi = psi_norm * np.exp(-0.5*self.m*self.w*X**2) * H(np.sqrt(self.m*self.w)*X)
        return psi


class ExactQuarticDoubleWell(ExactSolution):
    """
    Class to compute exact solutions of a quartic double well oscillator.
    The constructor takes an instance of the appropriate potential class
    and uses it's parameters
    """

    def __init__(self, qo):
        if not isinstance(qo, QuarticDoubleWell):
            raise TypeError("You must pass an instance of a QuarticDoubleWell")
        self.w = qo.w
        self.m = qo.m

    def energy_level(self, n):
        pass


class ExactMorseOscillator(ExactSolution):
    """
    Class to compute exact solutions of a Morse oscillator.
    The constructor takes an instance of the appropriate potential class
    and uses it's parameters
    """

    def __init__(self, mo):
        if not isinstance(mo, MorseOscillator):
            raise TypeError("You must pass an instance of a MorseOscillator")
        self.D0 = mo.D0
        self.r0 = mo.r0
        self.a = mo.a
        self.m = mo.m
        # change of variables
        self.xe = self.a*self.r0
        self.lam = np.sqrt(2.0*self.m*self.D0)/self.a

    def energy_level(self, n):
        return 1.0 - 1.0/self.lam**2 * (self.lam - n - 0.5)**2

    def eigenfunction(self, n, X):
        z = 2*self.lam*np.exp(X - self.xe)
        psi_norm = np.sqrt(np.math.factorial(n) * (2*self.lam - 2*n -1)/np.math.gamma(2*self.lam -n))
        L = Laguerre.basis(n)
        psi = psi_norm * z**(self.lam - n - 0.5) * np.exp(-0.5*z**2) * L(z)
        return psi

